import numpy as np
import scipy
import matplotlib.pyplot as plt
from numpy import cos,sin
from matplotlib.ticker import FuncFormatter

############# Input Parameters #################################

# Young's Modulus of the material
E = 3e10 # (Pa)

# Fracture Energy
Gc = 120 # (N/m)

# Stress Limit
sigc = 3e6 # (N/m)

# Length of the bar
L = 0.2

# Critical Separation
wc = (2 * Gc) / (sigc)

###################################################################

#Characteristic length
lc = L/4

# Cohesive zone length
lch = (E * Gc) / (sigc * sigc)

#lambda
lmb = lc / lch

#Dm 
Dm = 0.7

#G function - Co-efficients
a = np.pi/2

#h function -  Co-efficients
c = 0.0
b = 0.0

#k
k = (E/lch)

#Critcial Energy release rate
Yc = (0.5*sigc*sigc)/(E)

#Critcial Energy release rate - cohesive
yc = (0.5*sigc*sigc)/(k)

#Nodes per characteristic length
N_lc = 5

#Number of vertices/nodes
N_v = int(L /(lc/N_lc)) - 1

#Number of elements
N_elements = N_v  - 1

#grid parameter
x = np.linspace(0.,L,N_v)

#Element size
dx = L/N_elements

######### Program Parameters #################################

#Number of increments
N_increments = 30

#maximum number of iterations
max_iter = 100

############# functions #################################

def get_strain(u) :
    return (u[1::2] - u[:-1:2])/dx

def get_u_jump(u):
     return u[2:-1:2] - u[1:-2:2]

#class gd_cohesive():
#    gdcohesive
#    def __eval__(d)

############## functions - g(d) #################################

def gd_cohesive(d) :
    return (1./d)-1.

def dgd_cohesive_dd(d) :
    return -1./d**2

############## functions - h(d) #################################

def hd_cohesive(d):
    return c * d**3 + b * d**2 + d

def dhd_cohesive_dd(d):
    return  3*c * d**2 + 2*b * d + 1

############## functions - G(D) ##################################

def GD_bulk(D):
    return (1-D)**2 / ((1-D)**2 + (a*sin(a*D)*(1-D)+ 1 - cos(a*D))/(lmb))

def dGD_bulk_dD(D):
    tmp = ((a*(1 - D)*sin(a*D) - cos(a*D) + 1)/lmb + (1 - D)**2)
    return -((1 - D)**2 * ((a**2 * (1 - D) * cos(a*D))/lmb - 2*(1 - D)))/tmp**2 - (2*(1 - D))/tmp

def test_dGD_bulk_dD(D):
    dGD_bulk_dD_num = np.diag(scipy.optimize.approx_fprime(D, GD_bulk))
    dGD_bulk_dD_exa = dGD_bulk_dD(D)
    print(dGD_bulk_dD_num)
    print(dGD_bulk_dD_exa)
    diffnorm = np.linalg.norm(dGD_bulk_dD_num - dGD_bulk_dD_exa)
    print('test GD_bulk_dD  : diff = ', diffnorm)

############## functions - H(D) ##################################
    
def HD_bulk(D):
    num = 4*Dm**4*(Dm**2*(D - 1)**2 + 2*(D - Dm)**2*(-D*a*sin(D*a) + a*sin(D*a) - cos(D*a) + 1)) - (2*D*(cos(D*a) - 1) + Dm*((D - 1)*(D - 2*Dm) - 2*cos(D*a) + 2))**2*(3*D**2*c + 2*D*Dm*b + Dm**2)
    den = (2*Dm**2*lmb*(2*D*(cos(D*a) - 1) + Dm*((D - 1)*(D - 2*Dm) - 2*cos(D*a) + 2))**2)
    return num/den

def dHD_bulk_dD(D) :
    # num = 4*Dm**4*(Dm**2*(D - 1)**2 + 2*(D - Dm)**2*(-D*a*sin(D*a) + a*sin(D*a) - cos(D*a) + 1)) - (2*D*(cos(D*a) - 1) + Dm*((D - 1)*(D - 2*Dm) - 2*cos(D*a) + 2))**2*(3*D**2*c + 2*D*Dm*b + Dm**2)
    # den = (2*Dm**2*lmb*(2*D*(cos(D*a) - 1) + Dm*((D - 1)*(D - 2*Dm) - 2*cos(D*a) + 2))**2)
    # dnumdD = 4*Dm**4*(2*(D - Dm)**2*(a**2*cos(a*D) - a**2*D*cos(a*D)) \
    #                 + 4*(D - Dm)*(a*sin(a*D) - a*D*sin(a*D) - cos(a*D) + 1)  \
    #                 + 2*(D - 1)*Dm**2) \
    #         -2*(Dm*(-2*cos(a*D) + (D - 1)*(D - 2*Dm) + 2) + 2*D*(cos(a*D) - 1))*(2*b*Dm + 3*c*D**2 + Dm**2)*(Dm*(2*a*sin(a*D) + 2*D - 2*Dm - 1) \
    #         - 2*a*D*sin(a*D) + 2*(cos(a*D) - 1)) - (Dm*(-2*cos(a*D) + (D - 1)*(D - 2*Dm) + 2)\
    #         + 2*D*(cos(a*D) - 1))**2*(2*b*Dm + 6*c*D)
    # ddendD = 4*lmb*Dm**2*(Dm*(-2*cos(a*D) + (D - 1)*(D - 2*Dm) + 2) + 2*D*(cos(a*D) - 1))*(2*a*(Dm - D)*sin(a*D) + 2*cos(a*D) + 2*D*Dm - 2 *Dm**2 - Dm - 2)
    # return (dnumdD*den - num*ddendD)/den**2
     
    cosad = cos(D*a)
    sinad = sin(D*a)

    num = ((2*D*(cosad - 1) + Dm*((D - 1)*(D - 2*Dm) - 2*cosad + 2))*(4*Dm**4*(Dm**2*(D - 1) + a**2*(1 - D)*(D - Dm)**2*cosad + 2*(-D + Dm)*(D*a*sinad - a*sinad + cosad - 1)) - (3*D*c + Dm*b)*(2*D*(cosad - 1) + Dm*((D - 1)*(D - 2*Dm) - 2*cosad + 2))**2 + (2*D*(cosad - 1) + Dm*((D - 1)*(D - 2*Dm) - 2*cosad + 2))*(3*D**2*c + 2*D*Dm*b + Dm**2)*(2*D*a*sinad - Dm*(2*D - 2*Dm + 2*a*sinad - 1) - 2*cosad + 2)) + (4*Dm**4*(Dm**2*(D - 1)**2 + 2*(D - Dm)**2*(-D*a*sinad + a*sinad - cosad + 1)) - (2*D*(cosad - 1) + Dm*((D - 1)*(D - 2*Dm) - 2*cosad + 2))**2*(3*D**2*c + 2*D*Dm*b + Dm**2))*(2*D*a*sinad - Dm*(2*D - 2*Dm + 2*a*sinad - 1) - 2*cosad + 2))
    den = (Dm**2*lmb*(2*D*(cosad - 1) + Dm*((D - 1)*(D - 2*Dm) - 2*cosad + 2))**3)
    return num/den

def test_dHD_bulk_dD(D):
    dHD_bulk_dD_num = np.diag(scipy.optimize.approx_fprime(D, HD_bulk))
    dHD_bulk_dD_exa = dHD_bulk_dD(D)
    print(dHD_bulk_dD_num)
    print(dHD_bulk_dD_exa)
    diffnorm = np.linalg.norm(dHD_bulk_dD_num - dHD_bulk_dD_exa)
    print('test GH_bulk_dD : diff = ', diffnorm)
 
############## Bulk Damage ###############################

def get_len_mat(x) : 
    return np.abs(np.broadcast_to(x, (x.shape[0], x.shape[0])) -np.repeat(x, x.shape[0]).reshape((-1, x.shape[0])))

len_mat = get_len_mat(x)

def get_Bulk_damage0(d):
    d_new = np.concatenate([np.zeros(1), d, np.zeros(1)])
    D_values = np.max((d_new- len_mat/lc)*Dm, axis=1)
    D_values[D_values < 0] = 0  
    return D_values

def get_Bulk_damage(d, centeronly = True): 
    D_values = get_Bulk_damage0(d)
    D_center = (D_values[:-1] + D_values[1:]) / 2
    if centeronly : return D_center
    D_overall = np.empty(2 * len(D_values) - 1)
    D_overall[0::2] = D_values
    D_overall[1::2] = D_center
    return D_values[1:-1],D_center,D_overall

def get_dBulk_damage0_dd(d):
    d_new = np.concatenate([np.zeros(1), d, np.zeros(1)])
    n = len(d_new)
    D_values = (d_new- len_mat/lc)*Dm
    max_indexes = np.argmax( D_values, axis=1)    
    D_values = D_values[ np.arange(n), max_indexes]

    dD_values_dd_nz_col = max_indexes [D_values >= 0]
    dD_values_dd        = scipy.sparse.csr_array( (Dm*np.ones(len(dD_values_dd_nz_col)), (np.arange(n)[D_values >= 0] , dD_values_dd_nz_col )  ), (n,n))
    return dD_values_dd[:,1:-1]

def get_dBulk_damage_dd(d):
    dD_values_dd = get_dBulk_damage0_dd(d)
    dD_center_dd = (dD_values_dd[:-1,:] + dD_values_dd[1:, :]) / 2
    return dD_center_dd

def test_dBulk_damage_dd(d):
    dBulk_damage_dd_num = scipy.optimize.approx_fprime(d, get_Bulk_damage )
    dBulk_damage_dd_exa = get_dBulk_damage_dd(d)
    diffnorm = np.linalg.norm(dBulk_damage_dd_num - dBulk_damage_dd_exa.todense())
    print('test dBulk_damage  : diff = ', diffnorm)
    
###################### Equib Solve #####################

class linear_system_solve():

    def __init__(self):
        pass
    
    def get_K_uu_b(self, D_center) :
        matrix_values_D =  (GD_bulk(D_center)[:, np.newaxis]*(E/dx*np.array([1., -1., -1., 1.]))).flatten()
        if hasattr(self, 'K_uu_b') :
            self.K_uu_b.data = matrix_values_D
        else :
            col_indices = np.repeat(np.arange(0, (2*N_elements)).reshape((-1, 2)), 2, axis = 0).flatten()   
            row_indices = np.repeat(np.arange(0, (2*N_elements)),2)
            self.K_uu_b = scipy.sparse.coo_matrix((matrix_values_D, (row_indices, col_indices)),
                                       shape= (2*N_elements,2*N_elements) )
        return self.K_uu_b
    
    def get_K_uu_j(self):
        if not hasattr(self, 'K_uu_j') : 
            data = np.repeat(np.array([[ -k, k, k, -k]]), N_elements-1, axis = 0).flatten()     
            col_indices = np.repeat(np.arange(1, (2*N_elements-1)).reshape((-1, 2)), 2, axis = 0).flatten()    
            row_indices = np.repeat(np.arange(1, (2*N_elements)-1),2)
            self.K_uu_j = scipy.sparse.coo_matrix((data, (row_indices, col_indices)),
                                        shape= (2*N_elements,2*N_elements))  
        return self.K_uu_j
     
#    def K_uu_block(self, d) :
#        return self.get_bmatrix(d) +  self.get_jmatrix()

    def get_K_ul(self) :
        if not hasattr(self, "K_ul") :
            row_indices = np.repeat(np.arange(N_elements-1),2)
            col_indices = np.arange(1,(2*N_elements-1))
            data_kul = np.repeat([[-1,1]], N_elements-1, axis = 0).flatten()
            self.K_ul = scipy.sparse.coo_matrix((data_kul,(row_indices,col_indices)),
                                          shape= (N_elements-1,2*N_elements) )
        return self.K_ul

    def get_K_ll(self, d) :
        data  = -d/k
        if not hasattr(self, "K_ll" ) :
            row_indices = np.arange(len(data))
            col_indices = np.arange(len(data))
            self.K_ll = scipy.sparse.coo_matrix((data, (row_indices,col_indices)),
                                          shape= (len(data), len(data)) )
        else :
            self.K_ll.data = data
        return  self.K_ll
    
#    def get_dK_ll_dd(self, d) :
#        data  = -1/k*np/np.ones(len(d))
#        if not hasattr(self, "dK_ll_dd" ) :
#            row_indices = np.arange(len(data))
#            col_indices = np.arange(len(data))
#            self.dK_ll_dd = scipy.sparse.coo_matrix((data, (row_indices,col_indices)),
#                                          shape= (len(data), len(data)) )
#        else :
#            self.dK_ll_dd.data = data
#        return  self.dK_ll_dd
 
    def get_K_bc(self, imposed_displacements_keys):
        
        if not hasattr(self, "K_bc") :
            nc = len(imposed_displacements_keys)
            I = np.array( imposed_displacements_keys)
            J = np.arange(nc)
            Bval = np.ones(nc,dtype = 'float')
            self.K_bc = scipy.sparse.coo_matrix((Bval,(I,J)),shape =(2*N_elements,nc))
        return self.K_bc

    def get_K(self, d, D_center, imposed_displacements) :
        #K_uu_b = self.get_K_uu_b(d0)
        K_uu_b = self.get_K_uu_b(D_center)
        
        K_ll   = self.get_K_ll(d)
        if hasattr(self, "K") :
            self.K.data[:self.K_uu_b.nnz] = K_uu_b.data
            self.K.data[self.start_K_ll: self.end_K_ll] = K_ll.data   
        else :
            K_uu_j = self.get_K_uu_j()
            K_ul   = self.get_K_ul() 
            K_bc   = self.get_K_bc(list(imposed_displacements.keys()))
            data = np.hstack([K_uu_b.data, K_uu_j.data, K_ul.data, K_bc.data, \
                          K_ul.data, K_ll.data,\
                          K_bc.data])
            row_indices = np.hstack([K_uu_b.row, K_uu_j.row,  K_ul.col, K_bc.row, \
                                     K_ul.row+2*N_elements, K_ll.row+2*N_elements, \
                                     K_bc.col+2*N_elements+N_elements-1])
            col_indices = np.hstack([K_uu_b.col, K_uu_j.col, K_ul.row+2*N_elements, K_bc.col+2*N_elements + N_elements -1, \
                                     K_ul.col, K_ll.col +2*N_elements, \
                                     K_bc.row])
            size = 2*N_elements+(N_elements-1)+len(imposed_displacements.keys())
            self.K = scipy.sparse.coo_matrix((data, (row_indices, col_indices)),
                                          shape= (size, size) )
            self.start_K_ll = self.K_uu_b.nnz + self.K_uu_j.nnz + 2*self.K_ul.nnz + self.K_bc.nnz
            self.end_K_ll   = self.start_K_ll + self.K_ll.nnz
        return self.K
        
#    def get_dKdd(self, d, D_center, imposed_displacements) :
#        #K_uu_b = self.get_K_uu_b(d0)
#        dK_uu_b_dd = self.get_dK_uu_b_dd(D_center)
#        dK_ll_dd    = self.get_dK_ll_dd(d)
#        if hasattr(self, "K") :
#            self.dKdd.data[:self.dK_uu_b_dd.nnz] = dK_uu_b_dd.data
#            self.dKdd.data[self.dK_uu_b_dd.nnz:] = dK_ll_dd.data   
#        else :
#            data = np.hstack([dK_uu_b_dd.data, \
#                              dK_ll_dd.data])
#            row_indices = np.hstack([dK_uu_b_dd.row,  \
#                                     dK_ll_dd.row+2*N_elements ])
#            col_indices = np.hstack([dK_uu_b_dd.col, \
#                                     dK_ll_dd.col +2*N_elements])
#            size = 2*N_elements+(N_elements-1)+len(imposed_displacements.keys())
#            self.dKdd = scipy.sparse.coo_matrix((data, (row_indices, col_indices)),
#                                          shape= (size, size) )
#        return self.dKdd

    def F_u_vector (self, imposed_displacements):
        
        if not hasattr(self, "F"):
            self.F = np.zeros(3*N_elements-1 + len( imposed_displacements))    
        self.F[3*N_elements-1: ] = np.array(list(imposed_displacements.values()))
        
        return self.F

    def Solve_Equib_ul(self, d, D_center, imposed_displacements) :
        K = self.get_K(d, D_center, imposed_displacements)
        F = self.F_u_vector(imposed_displacements)
        u = scipy.sparse.linalg.spsolve(K,F)
        
        resx   = u[:(2*N_elements)]
        resL_n = u[(2*N_elements):(2*N_elements + N_elements-1)]
        resL_b = -u[(2 * N_elements + N_elements-1):]
    
        return resx, resL_b, resL_n, K
    
################# Solve for d #########################

def term_1_ugl(strain, D_center):
    return .5*dx*E*(GD_bulk(D_center).dot( strain**2))

def dterm_1_ugl_dD(strain, D_center):
    return .5*dx*E*(dGD_bulk_dD(D_center)*(strain**2))

def term_2_ugc(u_jump,d):
    return .5 * k * (gd_cohesive(d).dot( u_jump**2 ))
    
def dterm_2_ugc_dd(u_jump,d):
    return .5 * k * (dgd_cohesive_dd(d) * u_jump**2)

def Gc_hd(d):
    return np.sum(yc * hd_cohesive(d))

def dGc_hd_dd(d):
    return yc * dhd_cohesive_dd(d)

def Yc_hd(D_center):   
    return np.sum(Yc * dx * HD_bulk(D_center))

def dYc_hd_dD(D_center):   
    return Yc * dx * dHD_bulk_dD(D_center)

def term_4_uld(u_jump):
    return 0.5*k*np.sum(u_jump**2)
   
def term_5_uld(u_jump,lmb):
    return np.dot(lmb, u_jump) 
    
def term_6_uld(lmb,d):
    return .5/k* d.dot(lmb**2)

def dterm_6_uld_dd(lmb,d):
    return  .5/k*lmb**2

def clip_functional_ud(d, D_center, u, lmb,returnallterms = False) :
    strain = get_strain(u)
    u_jump = get_u_jump(u)
    term_1 = term_1_ugl(strain, D_center)
    term_2 = term_4_uld(u_jump)
    term_3 = term_5_uld(u_jump,lmb)
    term_4 = term_6_uld(lmb,d)
    term_5 =  Yc_hd( D_center)
    term_6 =  Gc_hd(d)

    term_2_ud = np.sum(0.5 * k * gd_cohesive(d)*u_jump**2)

    functional = term_1 - term_2 + term_3 - term_4 + term_5 + term_6
    if returnallterms : return functional,term_1,term_2,term_3,term_4,term_5,term_6,term_2_ud
    return functional

def dclip_functional_ud_dd(d, D_center, u, lmb) :
    strain = get_strain(u)
    dterm_1_dD = dterm_1_ugl_dD(strain, D_center)
    dterm_4_dd  = dterm_6_uld_dd(lmb,d)
    dterm_5_dD =  dYc_hd_dD( D_center)
    dterm_6_dd =  dGc_hd_dd(d)
    dD_center_dd = get_dBulk_damage_dd(d)
    dfunctional_dd = dD_center_dd.T.dot(dterm_1_dD + dterm_5_dD) - dterm_4_dd+dterm_6_dd
    return dfunctional_dd
    
def clip_functional_ul_d(linsys, d, u_constraint):
    D_center = get_Bulk_damage(d)
    u_, F, lmb_, K  = linsys.Solve_Equib_ul(d, D_center, u_constraint)  
    functional = clip_functional_ud(d, D_center, u_, lmb_)
    return functional

def jac_clip_functional_ul_d(linsys, d, u_constraint):
    D_center = get_Bulk_damage(d)
    u, F, lmb, K  = linsys.Solve_Equib_ul(d, D_center, u_constraint)  
    d_functional_dd = dclip_functional_ud_dd(d, D_center, u, lmb)
    return d_functional_dd

def test_jac_clip_functional_ul_d(linsys, d, u_constraint):
    def fun(d) : return clip_functional_ul_d(linsys, d, u_constraint)
    jac_num = scipy.optimize.approx_fprime(d, fun)
    jac_exa = jac_clip_functional_ul_d(linsys, d, u_constraint)
    #print(jac_num)
    #print(jac_exa)
    print(np.linalg.norm(jac_num-jac_exa))
    
def Solve_functional_ul_d(linsys, d_fun, dp_fun, u_constraint):
    functional = lambda d : clip_functional_ul_d(linsys, d, u_constraint)
    jac        = lambda d : jac_clip_functional_ul_d(linsys, d, u_constraint)
    bounds = scipy.optimize.Bounds(dp_fun,np.ones(len(dp_fun)))
    Damage_opt = scipy.optimize.minimize(
        fun = functional,
        jac = jac,
        x0 =d_fun,
        bounds = bounds,
        method = 'SLSQP',
        #tol = 1.e-5
        #method = 'Newton-CG', # no bounds for Newton-CG
        #method = 'L-BFGS-B'
    )    
    if not Damage_opt.success :
        raise Exception('Convergence failed')
    return Damage_opt

################# Profiling tools ##################

import pstats
class profile():

    import cProfile

    doprofile = True
    pr = cProfile.Profile()
    def enable():  
        if profile.doprofile : profile.pr.enable()
    def disable(): 
        if profile.doprofile :  profile.pr.disable()
    def print_stats(): 
        #if profile.doprofile : profile.pr.print_stats(sort='cumulative')
        if profile.doprofile : 
            #profile.pr.print_stats(sort='cumulative')
            stats = pstats.Stats(profile.pr)
            stats.sort_stats('cumulative').print_stats(.1)

################# main ##################

def main(incs, doprofile = False):
 
    profile.doprofile       = doprofile
    plt.close('all')

    inc = 0.
    
    u_fun =  np.zeros(2 * N_elements)
    u_constraint = {0:0. , (2 * N_elements)-1 : 0.}

    dp_fun = np.zeros(N_elements - 1)
    d_fun = np.zeros(N_elements - 1) 

    lmb_fun = np.zeros(N_elements - 1)

    u_fun_str = []
    d_fun_str = []
    bulk_d_str = []
    lmb_fun_str = []

    stress_fun_str = []
    jump_fun_str= []

    term1_str = []
    term2_str = []
    term3_str = []
    term4_str = []
    term5_str = []
    term6_str = []
    fun_str = []

    term_2_ud_str = []

    disp_ud = []
    Yc_hd_str = []
    Gc_hd_str = []

    bulk_centr_d_str = []
    hd_str = []
    bulk_overall_str = []

    total_dissipation_str = []
    term_dissipation_str = []
    cohesive_stress_str = []
    strain_str = []

    term1_diff_str = []
    term2_diff_str = []
    term3_diff_str = []
    term4_diff_str = []

    term12_diff_str = []
    term34_diff_str = []

    profile.enable()
    linsys = linear_system_solve()

    for inc, u_t in enumerate(incs) :
            
        u_constraint [(2 * N_elements)-1] = u_t
        
        if inc == 2 :
            dp_fun = np.zeros(N_elements - 1)
            dp_fun[int((N_elements -1)/2)] = 0.01
        else :
            dp_fun = d_fun.copy()
        
        print(" ------------------ ", inc)

        res = Solve_functional_ul_d(linsys, d_fun, dp_fun, u_constraint)

        d_fun = res.x
        D_fun_center = get_Bulk_damage(d_fun)
        u_fun,F_fun,lmb_fun,d_  = linsys.Solve_Equib_ul( d_fun, D_fun_center, u_constraint) 
        functional,term_1,term_2,term_3,term_4,term_5,term_6,term_2_ud = clip_functional_ud(d_fun, D_fun_center, u_fun,lmb_fun,returnallterms=True)

        #test_jac_clip_functional_ul_d(linsys, d_fun, u_constraint)

        cohesive_stress_str.append((k*get_u_jump(u_fun)*gd_cohesive(d_fun)))

        u_fun_str.append(u_fun)

        d_fun_str.append(d_fun)
        lmb_fun_str.append(lmb_fun)

        stress_fun_str.append(F_fun[-1])
        jump_fun_str.append(get_u_jump(u_fun))
        Yc_hd_str.append(Yc_hd(D_fun_center))
        Gc_hd_str.append(Gc_hd(d_fun))

        d1,d2,D_overall = get_Bulk_damage(d_fun, centeronly=False)
        bulk_d_str.append(d2)
        bulk_centr_d_str.append(d1)
        bulk_overall_str.append(D_overall)

        term1_str.append(term_1)
        term2_str.append(term_2)
        term3_str.append(term_3)
        term4_str.append(term_4)
        term5_str.append(term_5)
        term6_str.append(term_6)
        fun_str.append(functional)

        term_2_ud_str.append(term_2_ud)

        total_dissipation_str.append(term_5 + term_6)
        term_dissipation_str.append(term_1 + term_2_ud)

        term1_diff_str.append(((lc)/(2*lch*E))*((1/GD_bulk(Dm*d_fun))-1)) 
        term2_diff_str.append((dgd_cohesive_dd(d_fun))/(2*lch*k*gd_cohesive(d_fun)**2))
        term3_diff_str.append(np.sum((Yc*lc*HD_bulk(Dm*d_fun)/(lch*sigc**2))))
        term4_diff_str.append(np.sum((yc * dhd_cohesive_dd(d_fun))/(lch*sigc**2)))


        strain_str.append(get_strain(u_fun))

    profile.disable()

    profile.print_stats()
    return stress_fun_str,d_fun_str,bulk_overall_str,Gc_hd_str,Yc_hd_str, u_fun_str,jump_fun_str,cohesive_stress_str,strain_str,term1_str,term_2_ud_str,term5_str,term6_str,total_dissipation_str,term_dissipation_str,term1_diff_str,term2_diff_str,term3_diff_str,term4_diff_str

def pure_czm():

    exact_f = [0,sigc, 0 ]
    exact_inc = [0,(sigc*L)/E,wc]

    return exact_inc, exact_f

def Exact_Solution(ut):

    cric = (sigc * L)/(E * wc)
    u_exact = np.zeros(2 * N_elements)

    if cric < 1:

        term_1 =  (ut - (sigc * L)/(E))
        term_2 =  (1 - cric)

        if term_1<=0:
            w = 0
        else:
            w = term_1/term_2

        d = (k * w) / (sigc - ((sigc * w)/(wc)) + (k * w))

        if d > 1 :
            d = 1
        
        if w == 0 :
            F = (E  * ut)/(L)
            t = F
        else : 
            F = k * w * ((1/d) - 1)
            t = F

        u_exact[0] = 0.
        u_exact[1] = ( t * L) / (2 * E)
        u_exact[2] = u_exact[1] + w
        u_exact[3] = ut

    if cric >= 1:

        term_1 =  (ut - (sigc * L)/(E))
        term_2 =  (1 - cric)

        if term_1<=0:
            w = 0
        else:
            w = term_1/term_2
        
        d = (k * abs(w)) / (sigc - ((sigc * abs(w))/(wc)) + (k * abs(w)))

        if d > 1 :
            d = 1

        if w == 0 :
            F = (E  * ut)/(L)
            t = F
        else : 
            F = k * abs(w) * ((1/d) - 1)
            t = F
        u_exact[0] = 0.
        u_exact[1] = ( t * L) / (2 * E)
        u_exact[2] = u_exact[1] + w
        u_exact[3] = ut

        if term_1>0:
            x = ut

            ut = (sigc * L)/(E) - (x - (sigc * L)/(E) )

            if F == 0:
                ut = -(w)

            if ut>x:
                ut=x
         
    return w,d,t,F,u_exact,ut

def Exact():
    F = np.zeros(3)
    ut = np.zeros(3)
    l_critical = (E*wc)/sigc

    ratio = L/l_critical

    if ratio < 1 :
        F[0] = 0
        F[1] = sigc
        F[2] = 0

        ut[0] = 0
        ut[1] = (sigc*L)/(E)
        ut[2] = wc
    
    if ratio >= 1:
        F[0] = 0
        F[1] = sigc
        F[2] = 0


        ut[0] = 0
        ut[1] = (sigc*L)/(E)
        ut[2] = L*sigc*(3*E*wc - 2*L*sigc)/(E*(2*E*wc - L*sigc))

    return F,ut

def postProcessingDissipation(step_elem_strain, step_stress):
    ''' input : step_elemen_strain np.array of shape (nstep, nelem)  that contain the strain for each step and each element 
                step_stress ....    
    '''
    # Sachin must finish that ...
    step_elem_strain 
    step_stress  
    totalbulkdisp = 0.
    total_bulk_str = []
    sigm = (step_stress[1:] + step_stress[:-1])/2.
                
    for ie in range(step_elem_strain.shape[1]):
            deps = step_elem_strain[1:,ie] - step_elem_strain[:-1,ie]
            bulkdispe = dx*np.sum(deps*sigm)                                                        
            totalbulkdisp += bulkdispe
            total_bulk_str.append(bulkdispe)
    
    tot_bulk.append(total_bulk_str)
    step_w = np.array(jump_fun_str)
    # dstep_w = step_w[1:, 10] - step_w[:-1, 10]
    totalcohesivedisp = 0
    total_cohesive_str = []
    for ie in range(step_w.shape[1]):
        dstepw = step_w[1:, ie] - step_w[:-1, ie]
        cohesivedispe = np.sum(dstepw*sigm)
        totalcohesivedisp += cohesivedispe
        total_cohesive_str.append(cohesivedispe)
    
    tot_cohesive.append(total_cohesive_str)


def stress_plot(incs,stress_fun_str,exact_inc,exact_f):
    def format_x_ticks(value, pos):
            return f"{value:.1e}"

    plt.figure()
    plt.plot(incs,stress_fun_str,'x-',label = 'Numerical  - $F(u,\lambda,d)$')
    plt.plot(exact_inc,exact_f,color = 'black',label = "Exact Solution")
    plt.axhline(y=sigc, color='red', linestyle='--')
    plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='bottom', ha='right')
    plt.xlabel("Imposed Displacement (m)")
    plt.ylabel("Stress (MPa) ")
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.title("Stress($\sigma$) vs Imposed Displacement ($u_t$) ")
    plt.legend()
    plt.grid(True)
    plt.show()

def damage_plots(d_fun_str,bulk_overall_str):

    # Plot for Cohesive Damage
    plt.figure()
    plt.subplot(1, 2, 1)
    for i, d_data in enumerate(d_fun_str):
        plt.scatter(np.linspace(0, L, len(d_data)), d_data, marker='o', label=f'Increment {i}')
    plt.xlabel('Position along the bar [m]', fontsize = 'large')
    plt.ylabel('Cohesive Damage ', fontsize = 'large')
    plt.title('Cohesive Damage [d]', fontsize = 'large')
    plt.grid(True)

    # Plot for Bulk Damage
    plt.subplot(1, 2, 2)
    for i, d_data in enumerate(bulk_overall_str):
        plt.plot(np.linspace(0, L, len(d_data)), d_data, marker='o', label=f'Increment {i}')
    plt.xlabel('Position along the bar [m]', fontsize = 'large')
    plt.ylabel('Bulk Damage', fontsize = 'large')
    plt.axhline(y=Dm, color='red', linestyle='--')
    plt.text(L*0.1, Dm, '$D_m$', color='red', fontsize=10, va='bottom', ha='right')
    plt.title('Bulk Damage Along the Bar [D]', fontsize = 'large')
    plt.grid(True)

    plt.tight_layout() 
    plt.show()

def dissipation_plots(incs, Gc_ovr, Yc_ovr):

    def format_x_ticks(value, pos):
            return f"{value:.1e}"

    # Plot for Cohesive Damage
    plt.figure()
    plt.subplot(1, 2, 1)
    for i, d_data in enumerate(Gc_ovr):
        plt.plot(incs, d_data,label=f' c = {Dm_arr[i]}, b = {Dm_arr[i]}')
    plt.xlabel('Imposed Displacement [m]', fontsize = 'large')
    plt.ylabel('Cohesive Dissipation [N/m]', fontsize = 'large')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=Gc, color='red', linestyle='--')
    plt.text(1e-5, Gc, r'$G_c$', color='red', fontsize=10, va='bottom', ha='right')
    plt.title('Cohesive Dissipation', fontsize = 'large')
    plt.legend(fontsize = 'large')
    plt.grid(True)


    # Plot for Bulk Damage
    plt.subplot(1, 2, 2)
    for i, d_data in enumerate(Yc_ovr):
        plt.plot(incs, d_data, label=f' c = {Dm_arr[i]}, b = {Dm_arr[i]}')
    plt.xlabel('Imposed Displacement [m]', fontsize = 'large')
    plt.ylabel('Bulk Dissipation [N/m]', fontsize = 'large')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=Gc, color='red', linestyle='--')
    plt.text(1e-5, Gc, r'$G_c$', color='red', fontsize=10, va='bottom', ha='right')
    plt.title('Bulk Dissipation', fontsize = 'large')
    plt.legend(fontsize = 'large')
    plt.grid(True)

    plt.tight_layout() 
    plt.show()

def cohesive_stress(incs,jump_fun_str,cohesive_stress_str):
    def format_x_ticks(value, pos):
            return f"{value:.1e}"
    # for i, (jump, stress) in enumerate(zip(jump_fun_str, cohesive_stress_str)):
    #     plt.plot(jump, stress, label=f'Curve {i+1}')
    sig = [sigc,0]
    w = [0,wc]
    plt.plot(jump_fun_str, cohesive_stress_str,marker = 'x')
    plt.plot(w,sig)
    plt.xlabel("Cohesvie zone opening [m]", fontsize = 'large')
    plt.ylabel("Stress [Pa] ", fontsize = 'large')
    plt.axhline(y=sigc, color='red', linestyle='--')
    plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='bottom', ha='right')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.title("Cohesive Stress [$\sigma$] vs Opening [$\omega$]", fontsize = 'large')
    #plt.legend(fontsize = 'large')
    plt.grid(True)
    plt.show()

def strain_element(element_1_strain,element_2_strain,stress_fun_str):
    print(element_1_strain)
    plt.plot(element_1_strain,stress_fun_str)
    plt.show()

def stress_plots_overall(incs,str_ovr,exact_inc,exact_f):

    def format_x_ticks(value, pos):
            return f"{value:.1e}"
    plt.plot(exact_inc,exact_f,label = 'Exact Solution',color = 'black')
    for i, data in enumerate(str_ovr):
        #plt.plot(incs, data, label=f' $\\ell_c=$ {le_1[i]}')

        plt.plot(incs, data, label = f'CLIP',marker = 'x')

    plt.xlabel("Imposed Displacement [m]", fontsize = 'large')
    plt.ylabel("Stress [Pa] ", fontsize = 'large')
    plt.axhline(y=sigc, color='red', linestyle='--')
    plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='bottom', ha='right')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.title("Stress [$\sigma$] vs Imposed Displacement [$u_t$]", fontsize = 'large')
    plt.legend(fontsize = 'large')
    plt.grid(True)
    plt.show()

def total_dissipation(incs, tot_ovr):

    def format_x_ticks(value, pos):
            return f"{value:.1e}"

    for i, d_data in enumerate(tot_ovr):
        plt.plot(incs, d_data,label=f' $\ell_c = L$')

    plt.xlabel('Imposed Displacement [m]', fontsize = 'large')
    plt.ylabel('Dissipation [N/m]', fontsize = 'large')
    plt.title('Total Dissipation', fontsize = 'large')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=Gc, color='red', linestyle='--')
    plt.text(1e-5, Gc, r'$G_c$', color='red', fontsize=10, va='bottom', ha='right')
    plt.grid(True)
    plt.legend(fontsize = 'large')
    plt.show()

def displacment_over_the_bar(u_fun_str):
    
    plt.figure(figsize=(8, 6))
    x_clip = np.linspace(0,L,N_elements+1)
    x_clip = np.repeat(x_clip,2)
    x_clip = np.delete(x_clip,-1)
    x_clip = np.delete(x_clip,1)

    for i, u_data in enumerate(u_fun_str):
        plt.plot(x_clip,u_data, )
    plt.xlabel('Nodes/element')
    plt.ylabel('Displacement (u)')
    plt.title('Displacement Along the Bar')
    plt.legend()
    plt.grid(True)
    plt.show()

def damage_comb_plot(incs, d_fun_str, bulk_overall_str):

    ij = [1,11,20,-1]
    bulk = ['0.25', '0.5', '0.75', '' ]
    print(incs[11],incs[20])

    for i,b in zip(ij, bulk):
        if i < len(d_fun_str) :
            d_data = d_fun_str[i]
            bulk_data = bulk_overall_str[i]
            plt.scatter(L/2, max(d_data) )
            plt.plot(np.linspace(0, L, len(bulk_data)), bulk_data, label = f'$u_t = {b} \omega_c$') 
            
    plt.axhline(y=Dm, color='red', linestyle='--')
    plt.text(L*0.05, Dm, '$D_m$', color='red', fontsize=10, va='bottom', ha='right')

    plt.xlabel('Position along the bar [m]',fontsize = 'large')
    plt.ylabel('Cohesive and Bulk Damage', fontsize = 'large')
    plt.legend(fontsize = 'large')
    plt.title('Damage along the bar',fontsize = 'large')
    plt.grid(True)
    plt.show()

def dissipation_in_one(incs,tot_ovr,Gc_ovr,Yc_ovr):
    def format_x_ticks(value, pos):
            return f"{value:.1e}"
    # Create a 2x2 grid
    plt.subplot2grid((2, 2), (0, 0))  # First plot in the first row
    for i, d_data in enumerate(Gc_ovr):
        plt.plot(incs, d_data,label=f' $D_m = ${Dm_arr[i]}')
    plt.xlabel('Imposed Displacement [m]', )
    plt.ylabel('Cohesive Dissipation [N/m]', )
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=Gc, color='red', linestyle='--')
    plt.text(1e-5, Gc, r'$G_c$', color='red', fontsize=10, va='top', ha='right')
    plt.title('Cohesive Dissipation', fontsize = 'large',fontweight = 'bold')
    #plt.legend(fontsize = 'large')
    plt.grid(True)

    plt.subplot2grid((2, 2), (0, 1))  # Second plot in the first row
    for i, d_data in enumerate(Yc_ovr):
        plt.plot(incs, d_data, label=f' $D_m = ${Dm_arr[i]}')
    plt.xlabel('Imposed Displacement [m]', )
    plt.ylabel('Bulk Dissipation [N/m]', )
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=Gc, color='red', linestyle='--')
    plt.text(1e-5, Gc, r'$G_c$', color='red', fontsize=10, va='top', ha='right')
    plt.title('Bulk Dissipation', fontsize = 'large',fontweight = 'bold')
    #plt.legend(fontsize = 'large')
    plt.grid(True)

    plt.subplot2grid((2, 2), (1, 0), colspan=2)  # Centered plot in the second row
    for i, d_data in enumerate(tot_ovr):
        plt.plot(incs, d_data,label=f' $D_m = ${Dm_arr[i]}')

    plt.xlabel('Imposed Displacement [m]', )
    plt.ylabel('Dissipation [N/m]', )
    plt.title('Total Dissipation', fontsize = 'large',fontweight = 'bold')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=Gc, color='red', linestyle='--')
    plt.text(1e-5, Gc, r'$G_c$', color='red', fontsize=10, va='top', ha='right')
    plt.grid(True)
    plt.legend(fontsize = 'large')
    plt.show()

    plt.tight_layout()  # Adjust layout for better spacing
    plt.show()

def stress_all(incs,str_ovr,exact_inc,exact_f):
    
    def format_x_ticks(value, pos):
            return f"{value:.1e}"
    
    # Create a 2x2 grid
    plt.subplot2grid((2, 2), (0, 0))  # First plot in the first row
    plt.plot(exact_inc,exact_f,label = 'Exact Solution',color = 'black')
    plt.plot(incs, str_ovr[0],label=f' $h_e$ = {he_arr[i]}')
    plt.xlabel('Imposed Displacement [m]', )
    plt.ylabel('Cohesive Dissipation [N/m]', )
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=sigc, color='red', linestyle='--')
    plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='bottom', ha='right')
    plt.title('Cohesive Dissipation', fontsize = 'large',fontweight = 'bold')
    #plt.legend(fontsize = 'large')
    plt.grid(True)

    plt.subplot2grid((2, 2), (0, 1))  # Second plot in the first row
    plt.plot(exact_inc,exact_f,label = 'Exact Solution',color = 'black')
    plt.plot(incs, str_ovr[1],label=f' $h_e$ = {he_arr[i]}')
    plt.xlabel('Imposed Displacement [m]', )
    plt.ylabel('Bulk Dissipation [N/m]', )
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=sigc, color='red', linestyle='--')
    plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='bottom', ha='right')
    plt.title('Bulk Dissipation', fontsize = 'large',fontweight = 'bold')
    #plt.legend(fontsize = 'large')
    plt.grid(True)

    plt.subplot2grid((2, 2), (1, 0))  # Centered plot in the second row
    plt.plot(exact_inc,exact_f,label = 'Exact Solution',color = 'black')
    plt.plot(incs, str_ovr[2],label=f' $h_e$ = {he_arr[i]}')
    plt.xlabel('Imposed Displacement [m]', )
    plt.ylabel('Dissipation [N/m]', )
    plt.title('Total Dissipation', fontsize = 'large',fontweight = 'bold')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=sigc, color='red', linestyle='--')
    plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='bottom', ha='right')
    plt.grid(True)
    plt.legend(fontsize = 'large')
    plt.show()

    plt.subplot2grid((2, 2), (1, 1))  # Centered plot in the second row
    plt.plot(exact_inc,exact_f,label = 'Exact Solution',color = 'black')
    plt.plot(incs, str_ovr[3],label=f' $h_e$ = {he_arr[i]}')
    plt.xlabel('Imposed Displacement [m]', )
    plt.ylabel('Dissipation [N/m]', )
    plt.title('Total Dissipation', fontsize = 'large',fontweight = 'bold')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.axhline(y=sigc, color='red', linestyle='--')
    plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='bottom', ha='right')
    plt.grid(True)
    plt.legend(fontsize = 'large')
    plt.show()

    plt.tight_layout()  # Adjust layout for better spacing
    plt.show()

def stress_all_1(incs, str_ovr, exact_inc, exact_f, he_arr, sigc):
    
    def format_x_ticks(value, pos):
        return f"{value:.1e}"

    for i in range(len(he_arr)):
        # Create a 2x2 grid
        plt.subplot(2, 2, i + 1)
        plt.plot(exact_inc, exact_f, label='Exact Solution', color='black')
        plt.plot(incs, str_ovr[i], label=f'$h_e = \\frac{{\ell_c}}{{{he_arr[i]}}}$',marker = 'x')
        plt.xlabel("Imposed Displacement [m]", fontsize = 'large')
        plt.ylabel("Stress [Pa] ", fontsize = 'large')
        plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
        plt.axhline(y=sigc, color='red', linestyle='--')
        plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='top', ha='right')
        plt.title(f'$N_{{elements}} = {int(4*he_arr[i]) - 1 -1}$', fontsize='large', fontweight='bold')
        plt.legend(fontsize='large')
        plt.grid(True)

    plt.tight_layout()  # Adjust layout to prevent overlap
    plt.suptitle("Stress [$\sigma$] vs Imposed Displacement [$u_t$]", fontsize = 'large',fontweight = 'bold')
    plt.show()

def strain_plot(element_1_strain):

    for inc in range(len(element_1_strain)) :
        if inc%3 == 1:
            nelem = len(element_1_strain[inc])
            plt.plot(np.linspace(0.,L, nelem), element_1_strain[inc], label = str(inc))
    plt.xlabel('Position along the bar [m]',fontsize = 'large')
    plt.legend()
    plt.grid(True)
    plt.show()

def element_strain(stress_fun_str,element_1_strain):
    def format_x_ticks(value, pos):
            return f"{value:.1e}"
    step_elem_strain = np.array(element_1_strain)
    step_stress = np.array(stress_fun_str)
    
    for ie in range(step_elem_strain.shape[1]):
    #for ie in [18]:
            
        plt.plot(step_elem_strain[:, ie], step_stress,marker = 'x')
    plt.xlabel('Strain',fontsize = 'large')
    plt.ylabel("Stress [Pa]", fontsize = 'large')
    plt.axhline(y=sigc, color='red', linestyle='--')
    plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='bottom', ha='right')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.title("Stress [$\sigma$] vs Strain [$\epsilon$], Element-wise", fontsize = 'large')
    plt.grid(True)
    plt.legend()
    plt.show()

def term1_plot(incs,term1_str,term_2_ud_str):

    plt.plot(incs, term1_str,marker = 'x')
    plt.plot(incs,term_2_ud_str,marker = 'x')
    plt.xlabel("Imposed Displacement [m]", fontsize = 'large')
    plt.ylabel("Term 1 [N/m]", fontsize = 'large')
    plt.title("Term 1 vs Imposed displacement [$u_t$]")
    plt.grid(True)
    plt.show()

def term2_plot(incs,term_2_ud_str):

    # for i,d_data in enumerate(term_2_ud_str):
    #     plt.plot(np.linspace(0,L,N_elements-1), d_data,label = i)
    plt.plot(incs,term_2_ud_str,marker = 'x')
    plt.xlabel("Imposed Displacement [m]", fontsize = 'large')
    plt.ylabel("Term 2 [N/m]", fontsize = 'large')
    plt.title("Term 2 vs Imposed displacement [$u_t$]")
    plt.legend()
    plt.grid(True)
    plt.show()

def term5_plot(incs, term5_str):

    plt.plot(incs, term5_str,marker = 'x')
    plt.xlabel("Imposed Displacement [m]", fontsize = 'large')
    plt.ylabel("Term 3 [N/m]", fontsize = 'large')
    plt.title("Term 3 vs Imposed displacement [$u_t$]")
    plt.grid(True)
    plt.show()

def term6_plot(incs, term6_str):

    plt.plot(incs, term6_str,marker = 'x')
    plt.xlabel("Imposed Displacement [m]", fontsize = 'large')
    plt.ylabel("Term 4 [N/m]", fontsize = 'large')
    plt.title("Term 4 vs Imposed displacement [$u_t$]")
    plt.grid(True)
    plt.show()

def total_dissipation_plot(incs,total_dissipation_str):

    plt.plot(incs, total_dissipation_str)
    plt.xlabel("Imposed Displacement [m]", fontsize = 'large')
    plt.ylabel("Total dissipation [N/m]", fontsize = 'large')
    plt.title("Total dissipation vs Imposed displacement [$u_t$]")
    plt.grid(True)
    plt.show()

def term_dissipation_plot(incs,term_dissipation_str):

    # for i, d_data in enumerate(term_dissipation_str):
    #     plt.plot(np.linspace(0,L,N_elements-1), d_data)
    plt.plot(incs,term_dissipation_str)
    plt.plot(incs, term_dissipation_str)
    plt.xlabel("Imposed Displacement [m]", fontsize = 'large')
    plt.ylabel("Term dissipation [N/m]", fontsize = 'large')
    plt.title("Term dissipation vs Imposed displacement [$u_t$]")
    plt.grid(True)
    plt.show()

def jump_plot(incs,jump_fun_str):

    for i, d_data in enumerate(jump_fun_str):
        plt.plot(np.linspace(0,L,N_elements-1), d_data)
    plt.xlabel("Imposed Displacement [m]", fontsize = 'large')
    plt.ylabel("Jump [m]", fontsize = 'large')
    plt.title("Jump vs Imposed displacement [$u_t$]")
    plt.grid(True)
    plt.legend()
    plt.show()

def total_cohesive_dissip(total_cohesive_str):

    plt.plot(np.linspace(0,L,N_elements-1), total_cohesive_str)
    plt.xlabel('Position along the bar [m]', fontsize = 'large')
    plt.ylabel('Cohesive zone dissipation [N/m]',fontsize = 'large')
    plt.title("Cohesive zone dissipation along the bar",fontsize = 'large')
    plt.grid(True)
    plt.show()

def total_bulk_dissip(total_bulk_str):

    plt.plot(np.linspace(0,L,N_elements), total_bulk_str)
    plt.xlabel('Position along the bar [m]', fontsize = 'large')
    plt.ylabel('Dissipation [N/m]',fontsize = 'large')
    plt.title("Element-wise dissipation along the bar ")
    plt.grid(True)
    plt.show()

def total_cohesive_bulk_dissip(total_cohesive_str,total_bulk_str):
    plt.subplot(1, 2, 1)
    plt.plot(np.linspace(0,L,N_elements-1), total_cohesive_str, )
    plt.xlabel('Position along the bar [m]', fontsize = 'large')
    plt.ylabel('Cohesive zone dissipation [N/m]',fontsize = 'large')
    plt.title("Cohesive zone dissipation ",fontsize = 'large')
    plt.grid(True)
    plt.legend()

    plt.subplot(1, 2, 2)
    plt.plot(np.linspace(0,L,N_elements), total_bulk_str,  color='orange')
    plt.xlabel('Position along the bar [m]', fontsize = 'large')
    plt.ylabel('Dissipation [N/m]',fontsize = 'large')
    plt.title("Element-wise dissipation ")
    plt.grid(True)
    plt.legend()

    # Adjust layout to prevent overlap
    plt.tight_layout()

    # Show the plots
    plt.show()

def total_dissip_in_one(tot_bulk,tot_cohesive):

    plt.subplot(1, 2, 1)
    for i,d_data in enumerate(tot_cohesive):
        area = np.sum(d_data)
        plt.plot(np.linspace(0,L,N_elements-1), d_data ,label = f'$Dp_{{Coh}} =${area:.2f}')
    plt.xlabel('Position along the bar [m]', fontsize = 'large')
    plt.ylabel('Cohesive zone dissipation [N/m]',fontsize = 'large')
    plt.title("Cohesive zone dissipation ",fontsize = 'large')
    plt.grid(True)
    plt.legend()

    plt.subplot(1, 2, 2)
    for i,d_data in enumerate(tot_bulk):
        area = np.sum(d_data)
        plt.plot(np.linspace(0,L,N_elements), d_data,label = f'$a =$ {a1[i]} , $Dp_{{Bulk}} =${area:.2f}')
    
    plt.xlabel('Position along the bar [m]', fontsize = 'large')
    plt.ylabel('Dissipation [N/m]',fontsize = 'large')
    plt.title("Element-wise dissipation ")
    plt.grid(True)
    plt.legend()

    # Adjust layout to prevent overlap
    plt.tight_layout()

    # Show the plots
    plt.show()

def plot_tot_cohesive(coh_str,jump_coh_str):
    def format_x_ticks(value, pos):
            return f"{value:.1e}"
    
    sig = [sigc,0]
    w = [0,wc]

    for i,d_data in enumerate(coh_str):
        jump = jump_coh_str[i]
        d_area = d_data.copy()
        replacement = np.array([0,sigc])
        for j in range(len(np.isnan(d_area))):
            if np.isnan(d_area[j]):
                d_area[j] = replacement[j% len(replacement)]
        area = np.trapz(d_area,jump)
        label = f'$a = $ {a1[i]}, $Area = ${area:.2f}'
        plt.plot(jump, d_data, marker = 'x',label= label)
    plt.plot(w,sig,color = 'black')
    plt.xlabel("Cohesvie zone opening [m]", fontsize = 'large')
    plt.ylabel("Stress [Pa] ", fontsize = 'large')
    plt.axhline(y=sigc, color='red', linestyle='--')
    plt.text(8e-5, 3e6, 'Stress limit', color='red', fontsize=10, va='bottom', ha='right')
    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_x_ticks))
    plt.title("Cohesive Stress [$\sigma$] vs Opening [$\omega$]", fontsize = 'large')
    plt.legend(fontsize = 'large')
    plt.grid(True)
    plt.show()

if __name__ == '__main__':

    plt.close('all')
    incs = np.concatenate(([0], np.linspace((sigc*L)/E,wc*1.5,N_increments-1)))
    
    #Dm_arr = np.array([0.01,0.1, 0.3, 0.5, 0.7, 0.9,0.99])
    #Dm_arr = np.array([0.7])

    # he_arr = np.array([5,10,20,40])

    # c_arr = [ -0.5,  -0.3,  -0.1 ,0., 0.1, 0.3 ,0.5]
    # b_arr = [ -0.5,  -0.3,  -0.1 ,0., 0.1, 0.3, 0.5]

    #dissip_coh = [r"$0\%$",r"$20\%$",r"$40\%$",r"$50\%$",r"$60\%$",r"$80\%$", r"$100\%$", ]
    #dissip_bulk = [r"$100\%$",r"$80\%$",r"$60\%$",r"$50\%$",r"$40\%$",r"$20\%$", r"$0\%$", ]

    a_values = [np.pi/2,np.pi/3, np.pi/4,np.pi/6, np.pi/8, np.pi/12, np.pi/20]
    a1 = [r'$\frac{\pi}{2}$',r'$\frac{\pi}{3}$',r'$\frac{\pi}{4}$', r'$\frac{\pi}{6}$', r'$\frac{\pi}{8}$',r'$\frac{\pi}{12}$',r'$\frac{\pi}{20}$']

    #le_arr = [1,2,4,8,16]
    #le_1 = [r'$L$',r'$\frac{L}{2}$',r'$\frac{L}{4}$',r'$\frac{L}{8}$',r'$\frac{L}{16}$']

    str_ovr = []
    d_ovr = []
    bulk_ovr = []
    Gc_ovr = []
    Yc_ovr = []
    tot_ovr = []
    tot_bulk = []
    tot_cohesive = []
    cohesive_str = []
    coh = []
    coh_str = []
    max_d = []
    jump_coh= []
    jump_coh_str = []
    
    for i in range(len(a_values)):
        # c = c_arr[i]
        # b = b_arr[i]
        max_d_str = []

        #Dm = Dm_arr[i]

        a = a_values[i]

        #le = le_arr[i]

        # lc = L/le
        # lmb = lc / lch
        # #N_lc = he_arr[i]
        # N_v = int(L /(lc/N_lc)) - 1
        # N_elements = N_v  - 1
        # x = np.linspace(0.,L,N_v)
        # dx = L/N_elements
        # len_mat = get_len_mat(x)
     
        stress_fun_str,d_fun_str,bulk_overall_str,Gc_hd_str,Yc_hd_str, u_fun_str,jump_fun_str,cohesive_stress_str,strain_str,term1_str,term_2_ud_str,term5_str,term6_str,total_dissipation_str,term_dissipation_str,term1_diff_str,term2_diff_str,term3_diff_str,term4_diff_str = main(incs)
        exact_inc, exact_f = pure_czm()
        F,ut = Exact()

        max_d_str.extend([np.max(arr) for arr in d_fun_str])
        jump_coh = [arr[int((N_elements - 1)/2)] for arr in jump_fun_str]

        max_d.append(max_d_str)
        jump_coh_str.append(jump_coh)
        coh = (k*np.array(jump_coh)*gd_cohesive(np.array(max_d_str)))
        coh_str.append(coh)

        str_ovr.append(stress_fun_str)
        d_ovr.append(d_fun_str)
        bulk_ovr.append(bulk_overall_str)
        cohesive_str.append(cohesive_stress_str)

        step_elem_strain = np.array(strain_str)
        step_stress = np.array(stress_fun_str)
        totalbulkdisp = 0.
        total_bulk_str = []
        sigm = (step_stress[1:] + step_stress[:-1])/2.
                    
        for ie in range(step_elem_strain.shape[1]):
                deps = step_elem_strain[1:,ie] - step_elem_strain[:-1,ie]
                bulkdispe = dx*np.sum(deps*sigm)                                                        
                totalbulkdisp += bulkdispe
                total_bulk_str.append(bulkdispe)
        
        tot_bulk.append(total_bulk_str)
        step_w = np.array(jump_fun_str)
        # dstep_w = step_w[1:, 10] - step_w[:-1, 10]
        totalcohesivedisp = 0
        total_cohesive_str = []
        for ie in range(step_w.shape[1]):
            dstepw = step_w[1:, ie] - step_w[:-1, ie]
            cohesivedispe = np.sum(dstepw*sigm)
            totalcohesivedisp += cohesivedispe
            total_cohesive_str.append(cohesivedispe)
        
        tot_cohesive.append(total_cohesive_str)

        # total_cohesive_dissip(total_cohesive_str)
        # total_bulk_dissip(total_bulk_str)
        # total_cohesive_bulk_dissip(total_cohesive_str,total_bulk_str)
        
        print("Total Cohesive Dissipation",totalcohesivedisp)
        print("Total Bulk Dissipation",totalbulkdisp)
        print("Total Dissipation",totalcohesivedisp+totalbulkdisp)
        
        print("Gc",Gc)
        print("Difference", Gc - totalcohesivedisp - totalbulkdisp)
    

        Gc_ovr.append(Gc_hd_str)
        Yc_ovr.append(Yc_hd_str)
        tot_ovr = [[a + b for a, b in zip(sublist1, sublist2)] for sublist1, sublist2 in zip(Gc_ovr, Yc_ovr)]
    
    # sig = [sigc,0]
    # w = [0,wc]
    # coh_array = coh[0]
    # replacement = np.array([0,sigc])
    # for i in range(len(np.isnan(coh_array))):
    #  if np.isnan(coh_array[i]):
    #     coh_array[i] = replacement[i % len(replacement)]


    # plt.plot(jump_coh,coh_array,marker = 'x')
    # plt.plot(w,sig,color = 'black')
    # plt.grid(True)
    # plt.show()
    # print(jump_coh)
    # print(coh_array)

    # area = np.trapz(coh_array,jump_coh)
    # print("area",area)

    total_dissip_in_one(tot_bulk,tot_cohesive)
    plot_tot_cohesive(coh_str,jump_coh_str)
    # stress_plots_overall(incs,str_ovr,exact_inc,exact_f)
    # dissipation_in_one(incs,tot_ovr,Gc_ovr,Yc_ovr)
    # total_dissipation(incs, (tot_ovr))
    # stress_plots_overall(incs,str_ovr,ut,F)
    # dissipation_plots(incs, Gc_ovr, Yc_ovr)
    stress_plot(incs,stress_fun_str,exact_inc,exact_f)
    # damage_plots(d_fun_str,bulk_overall_str)
    # displacment_over_the_bar(u_fun_str)
    # damage_comb_plot(incs, d_fun_str, bulk_overall_str)
    # stress_all(incs,str_ovr,exact_inc,exact_f)
    # strain_element(element_1_strain,element_2_strain,stress_fun_str)
    # stress_all_1(incs, str_ovr, exact_inc, exact_f, he_arr, sigc)
    # strain_plot(strain_str)
    # element_strain(stress_fun_str,strain_str)
    # term1_plot(incs,term1_str,term_2_ud_str)
    # term2_plot(incs,term_2_ud_str)
    # term5_plot(incs, term5_str)
    # term6_plot(incs, term6_str)
    # total_dissipation_plot(incs,total_dissipation_str)
    # term_dissipation_plot(incs,term_dissipation_str)
    # jump_plot(incs,jump_fun_str)
    
    cohesive_stress(incs,jump_fun_str,cohesive_stress_str)
    #print(cohesive_stress_str)
    
    # print(len(jump_fun_str))
    # for arr in jump_fun_str:
    #     arr_shape = arr.shape
    #     print(f"Shape of the array: {arr_shape}")

    
    # totaldispe = 0
    # for ie in range(step_elem_strain.shape[1]):
    #     deps = step_elem_strain[1:,ie] - step_elem_strain[:-1,ie]
    #     dstepw = step_w[1:, ie] - step_w[:-1, ie]
    #     bulkdispe = dx*np.sum(deps*sigm)
    #     cohesivedispe = np.sum(dstepw*sigm)
    #     totaldispe+= (cohesivedispe+bulkdispe)
    #     print(totaldispe)


    #dstep_w = step_w[1:, int((N_elements/2) - 1)] - step_w[:-1, int((N_elements/2) - 1)]
    
    #totalcohesivedisp = np.sum(dstep_w * sigm)
    
    
    


    
    

    